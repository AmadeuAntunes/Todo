﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using ToList.View;
using Xamarin.Forms;

namespace ToList.ViewModel
{
    public class ToDoViewModel : BaseViewModel
    {
        public ICommand EnviarTarefaCommand
        {
            get
            {
                return new RelayCommand(EnviarTarefa);
            }
        }

        private async void EnviarTarefa()
        {
            MainViewModel.GetInstance().Todo = new ToDoViewModel();
            await Application.Current.MainPage.DisplayAlert("Error", Tarefa, "OK");
            await Application.Current.MainPage.Navigation.PushAsync(new PageTarefa());
        }


        string _tarefa;

        public string Tarefa
        {
            get { return _tarefa; }

            set
            {
                SetValue(ref this._tarefa, value);
            }
        }

       
    }
}
