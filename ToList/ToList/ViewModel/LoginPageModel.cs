﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using ToList.View;
using Xamarin.Forms;

namespace ToList.ViewModel
{
    public class LoginViewModel : BaseViewModel
    {

        #region Events

        // public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Atributes

        private bool _isRunning;
        private bool _isRemebered;
        private bool _isEnable;
        private string _email;
        private string _password;


        #endregion

        #region Properties

        public bool IsRunning
        {
            get { return _isRunning; }

            set
            {
                SetValue(ref this._isRunning, value);
            }
        }

        public bool IsRemebered
        {
            get { return _isRemebered; }

            set
            {
                SetValue(ref this._isRemebered, value);
            }
        }

        public bool IsEnable
        {
            get { return _isEnable; }

            set
            {
                SetValue(ref this._isEnable, value);
            }
        }

        public string Email
        {
            get { return _email; }

            set
            {
                SetValue(ref this._email, value);
            }
        }

        public string Password
        {
            get { return _password; }

            set
            {
                SetValue(ref this._password, value);
            }
        }
        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "You must enter email, please", "OK");
                return;
            }
            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "You must enter password, please", "OK");
                return;
            }

            this.IsRunning = true;
            this.IsEnable = false;

            if (this.Email != "rafa@gmail.com" || this.Password != "123")
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Email or password incorrect!", "OK");
                return;
            }


            this.Password = string.Empty;
            this.IsRunning = false;
            this.IsEnable = true;
            this.Email = string.Empty;

           MainViewModel.GetInstance().Todo = new ToDoViewModel();
           await Application.Current.MainPage.Navigation.PushAsync(new PageToDo());

        }

        #endregion






        #region Constructores

        public LoginViewModel()
        {

            this.IsRemebered = true;
            this.IsEnable = true;
        }

        #endregion


    }
}
