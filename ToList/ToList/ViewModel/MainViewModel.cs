﻿namespace ToList.ViewModel
{
    public class MainViewModel 
    {
        /*
         
        Neste ficheiro faço instancio O LoginModel
        Sempre que existir uma nova View tem de ser criada uma nova página
       
        */

        public LoginViewModel Login { get; set; }

        public ToDoViewModel Todo { get; set; }

        public  TarefaViewModel Tarefa { get; set; }
       

        #region ViewModels


        #endregion

        #region Constructores

        public MainViewModel()
        {
            instance = this;
            this.Login = new LoginViewModel();
        }

        #endregion

        #region Singleton

        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            else
            {
                return instance;
            }

        }

        #endregion


    }
}
