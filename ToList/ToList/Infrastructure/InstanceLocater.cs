﻿namespace ToList.Infrastructure
{
    using ViewModel;
    class InstanceLocater
    {
        #region Properties

        public MainViewModel Main { get; set; }

        #endregion

        #region Constructores

        public InstanceLocater()
        {
            this.Main = new MainViewModel();
        }
        #endregion
    }
}
